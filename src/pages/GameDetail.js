import React, { Component } from "react";
import NavbarComponent from "../components/NavbarComponent";
import Jumbotron from "../components/JumbotronComponent";
import Footer from "../components/Footer";
import firebase from "../services/firebase";
import GameDetailComponent from "../components/GameDetailComponent";
export default class GameDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      game: props.location.state,
      allUserData: null,
      isLoading: true,
      currentUser: null,
    };
    // console.log(this.state.game);
  }

  componentDidMount() {
    document.querySelector(".navbar").style.backgroundColor = "black";
    this.getAllUserData();
  }

  getAllUserData = async () => {
    try {
      const snapshot = await firebase
        .database()
        .ref("users")
        .orderByChild("gameHistory")
        .startAt(0)
        .once("value");
      const allUserData = snapshot.val();

      this.setState({
        isLoading: false,
        allUserData,
      });
    } catch (error) {
      console.log(error);
    }
  };

  get Loader() {
    return <em>Loading...</em>;
  }

  render() {
    const { allUserData, game } = this.state;
    return (
      <>
        <NavbarComponent />
        {this.state.isLoading ? (
          this.Loader
        ) : (
          <GameDetailComponent
            titleGame={game.name}
            bodyGame={game.detail}
            imgGame={game.img}
            linkGame={game.link}
            valueFromParent={allUserData}
          />
        )}
        <Footer />
      </>
    );
  }
}
