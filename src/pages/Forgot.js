import React, { Component, Fragment } from "react";
import firebase from "../services/firebase";
import classes from "../styles/Forgot.module.css";
import Input from "../components/Input";
import NavbarComponent from "../components/NavbarComponent";
import Footer from "../components/Footer";

export default class Forgot extends Component {
  state = {
    email: "",
  };

  set = (name) => (event) => {
    // console.log(event.target.value);

    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = async (event) => {
    const { email } = this.state;
    const { history } = this.props;

    event.preventDefault();

    if (!email) {
      return alert("email must be filled");
    }

    try {
      await firebase.auth().sendPasswordResetEmail(email);
      alert("Email has been sent to change your password");
      history.push("/login");
      console.log("success");
    } catch (error) {
      alert("error");
      console.log(error);
    }
  };

  componentDidMount() {
    document.querySelector(".navbar").style.backgroundColor = "black";
  }

  render() {
    return (
      <Fragment>
        <NavbarComponent />
        <div className={classes.Forgot}>
          <h1>Forgot Password</h1>
          <form onSubmit={this.handleSubmit}>
            <Input
              labelFor="Email"
              type="email"
              name="email"
              change={this.set("email")}
              value={this.state.email}
            />
            <Input type="submit" value="Reset Password" />
          </form>
        </div>
        <Footer />
      </Fragment>
    );
  }
}
