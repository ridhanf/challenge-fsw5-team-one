import React,{Component,Fragment} from 'react'
import Description from '../components/CardGameDescription'
import Leaderboard from '../components/CardGameLeaderboad'
import '../styles/GameDetail.css'
import { Container,Row,Col,Card,Table } from 'react-bootstrap';
export default class GameDetailComponent extends Component{
    constructor(props) {
        super(props)
        this.state = {
            titleGame:props.titleGame,
            bodyGame:props.bodyGame,
            imgGame:props.imgGame,
            linkGame : props.linkGame,
            data:''
        }
        // console.log(this.state.bodyGame)
    }
    get Content() {
        const data = this.props.valueFromParent
        const userIds = Object.keys(data)
        // console.log(userIds)
        return userIds.map((el, i) => {
            const detailData = data[el]
            const histori=detailData.gameHistory
            
            var key, count = 0;
                for(key in histori) {
                if(histori.hasOwnProperty(key)) {
                    count++;
                }
            }
            const histori1 = detailData.gameHistory[count-1][1]
            // console.log(histori1)
            return (
                <Fragment key={el}>
                    <tr>
                        <td>{detailData.username}</td>
                        {/* <td>{histori}</td> */}
                        <td>{histori1}</td>
                    </tr>
                </Fragment>
            )
        })
    }

    render() {
    return (
        <div>
            <Container className="py-5">
                    <Row>
                        <Col>
                            <Description titleGame={this.state.titleGame} bodyGame={this.state.bodyGame} imgGame={this.state.imgGame} linkGame={this.state.linkGame}/>
                        </Col>
                        <Col>
                        <Card>
                                <h1>All User Score</h1>
                                <Table  hover responsive>
                                    <thead>
                                        <tr>
                                        <th>Username</th>
                                        <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.Content
                                        }
                                    </tbody>
                                </Table> 
                                </Card>
                        </Col>
                    </Row>
                    </Container>
        </div>
    )
}
}
