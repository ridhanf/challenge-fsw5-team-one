import React, { useState } from "react";
import "./ButtonControl.css";
import ButtonRestart from "../../assets/images/restart.png";
import RulesButton from "../../assets/images/rules.png";
import Modal from "react-modal";

function ButtonControl() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    <div className="button-control">
      <button>
        <a href="/game/rock-paper-scissors">
          <img src={ButtonRestart} alt="button restart" />
        </a>
      </button>

      <button onClick={() => setModalIsOpen(true)}>
        <img src={RulesButton} alt="button rules" />
      </button>
      <Modal
        isOpen={modalIsOpen}
        style={{
          overlay: {
            background: "none",
          },
          content: {
            width: "420px",
            height: "280px",
            backgroundColor: "#F6D76D",
            color: "#4C1C12",
            textAlign: "center",
            border: "none",
          },
          button: {
            width: "100px",
            height: "50px",
          },
        }}
      >
        <h2>---- Rules Game ----</h2>
        <h5>🚀 1 round of 3x suits</h5>
        <h5>🚀 If you win, you get 10 points</h5>
        <h5>🚀 if you lose the score is less -5</h5>
        <p>** Klik "LEFT ARROW" to <strong>SAVE</strong> your point **<br/>
          !!! Klik "RESTART" will <strong>NOT SAVE</strong>not saved your point !!!
        </p>

        <div className="modal__rules">
          <button onClick={() => setModalIsOpen(false)}>Close</button>
        </div>
      </Modal>
    </div>
  );
}

export default ButtonControl;
