import React from "react";
import "./Header.css";
import LogoBattle from "../../assets/images/battle.png";
import backButton from "../../assets/images/back.png";
import {Link} from 'react-router-dom'

function HeaderGame() {
  return (
    <div className="header__game">
      <div className="header__title">
        <Link to ="/">
          <img src={backButton} alt="back button" />
        </Link>
        <img src={LogoBattle} alt="logo battle" />
      </div>
    </div>
  );
}

export default HeaderGame;
