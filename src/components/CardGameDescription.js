import React from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function CardGameDescription(props) {
    // console.log('nama game: '+props.titleGame+'\ndeskripsi game:'+props.bodyGame+'\nalamat gambar :'+props.img)
    return (
        <div>
            <Card>
                <h1>{props.titleGame}</h1>
                <p>{props.bodyGame}</p>
                    <img class="fakeimg" src={props.imgGame} style={{width:"100%"}}/>
                <Button variant="dark" className="mt-5"><Link to={props.linkGame}>Play Game</Link></Button>
                
            </Card>
        </div>
    )
}
