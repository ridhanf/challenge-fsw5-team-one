import React, { Component } from 'react'
import SectionTitle from './SectionTitle'
import TestimonialCard from './TestimonialCard'
import { Container } from 'react-bootstrap'
import classes from '../styles/Testimonial.module.css'

export default class Testimonial extends Component {
    
    render() {
        return (
            <Container fluid className={`${classes.scores}`}>
                {/* <div className="container vh-100"> */}
                    <div className={`${classes.vcenter} row d-flex justify-content-center align-items-center`}>
                        <div className="col-sm-md-lg-6 m-5 text-left">
                            <SectionTitle />
                        </div>
                        <div className="col-sm-md-lg-6 d-flex flex-column align-self-center">
                            <div className="p-2 text-left">
                                <TestimonialCard name="Evan Lathi" job="PC GAMERS" post="One of my gaming highlights of the year." />
                            </div>
                            <div className="p-2 text-left">
                                <TestimonialCard name="Jada Griffin" job="Nerdreactor" post="The next big thing in the world of streaming and survival games." />
                            </div>
                            <div className="p-2 text-left">
                                <TestimonialCard name="Aoron will" job="Approx" post="Snoop Dogg Playing The Wildly Entertaining ‘SOS’ Is Ridiculous." />
                            </div>
                        </div>
                    </div>
                {/* </div> */}
            </Container>
        )
    }
}
